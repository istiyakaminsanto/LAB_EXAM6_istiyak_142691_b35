<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit177657a6e5ec0d5c85866cd4ff8adaa4
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/BITM/SEIP142691',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit177657a6e5ec0d5c85866cd4ff8adaa4::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit177657a6e5ec0d5c85866cd4ff8adaa4::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
